<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier contient les réglages de configuration suivants : réglages MySQL,
 * préfixe de table, clefs secrètes, langue utilisée, et ABSPATH.
 * Vous pouvez en savoir plus à leur sujet en allant sur
 * {@link http://codex.wordpress.org/fr:Modifier_wp-config.php Modifier
 * wp-config.php}. C'est votre hébergeur qui doit vous donner vos
 * codes MySQL.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d'installation. Vous n'avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en "wp-config.php" et remplir les
 * valeurs.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define('DB_NAME', 'vatier');

/** Utilisateur de la base de données MySQL. */
define('DB_USER', 'root');

/** Mot de passe de la base de données MySQL. */
define('DB_PASSWORD', 'root');

/** Adresse de l'hébergement MySQL. */
define('DB_HOST', 'localhost');

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define('DB_CHARSET', 'utf8');

/** Type de collation de la base de données.
  * N'y touchez que si vous savez ce que vous faites.
  */
define('DB_COLLATE', '');

/**#@+
 * Clefs uniques d'authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clefs secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n'importe quel moment, afin d'invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'QsXbj~U5UJfiA7@dwH%+W34!)n[LxXDt~%u3k2YC=@M >&|- %1{ v.[x1U*]+Pf');
define('SECURE_AUTH_KEY',  'A&$D4)J#XK8sJ8tw-J<)H|ulU$(G9aWP+FV$6hIu<QmIF-Eo1;tl4L/V-f}DIHpH');
define('LOGGED_IN_KEY',    'h,TP|+&>+c]3pZ;28w.**Z4gp^%}[H3#&TatQaMeW!C9hRhzKO40:6LtBXF%Hx{+');
define('NONCE_KEY',        ')HAN5RUr+ (fAh~SA~YtaF|PClLC6W9F_wDCWZ!~KT7+uqSO20Zq)YNopz@*xv_B');
define('AUTH_SALT',        '++&!G!DI9/a+KTK+*0(^(|Wg,){:[+j!faObCoqd7|7VRpAuPCw{5`%2-4;/q|cm');
define('SECURE_AUTH_SALT', '}~A|`gat,j|ft>g!7|j1/+[NZOE6tgN#TkAb,Vo~VK3foKh)oMcS-95Y`)o9e8~W');
define('LOGGED_IN_SALT',   ':fYM)P%95M.N?l,BCbA5sPC.%=zo0xT&@F_!&`@l|r-51/L>jhg95=wnbsFAj~E2');
define('NONCE_SALT',       'C>sg.ITj29YTcb++ogO[S8Eb*gLVDT(XDLVAf1,$t;9A8VN34[Nhcx]i%UH[Q&0o');
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N'utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés!
 */
$table_prefix  = 'vt_';

/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l'affichage des
 * notifications d'erreurs pendant vos essais.
 * Il est fortemment recommandé que les développeurs d'extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 */
define('WP_DEBUG', false);

/* C'est tout, ne touchez pas à ce qui suit ! Bon blogging ! */

/** Chemin absolu vers le dossier de WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once(ABSPATH . 'wp-settings.php');