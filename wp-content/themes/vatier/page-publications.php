<?php
/**
 * Template Name: Template Publication
 *
 *
 * @package vatier
 */

get_header(); ?>

<?php $paged = (get_query_var('paged')) ? get_query_var('paged') : 1; ?>

<?php while ( have_posts() ) : the_post(); ?>

	<?php $post_thumbnail_id = get_post_thumbnail_id(); ?>
	<?php $post_thumbnail_url = wp_get_attachment_url( $post_thumbnail_id ); ?>
	<?php if(empty($post_thumbnail_url)): ?>
	<?php $post_thumbnail_url = get_template_directory_uri().'/images/contact.jpg'; ?>
	<?php endif; ?>
	<?php $subtitle = get_field('sous_titre'); ?>
	<?php $icone = get_field('icone'); ?>

	<div class="section-top-image">
		<div class="container inner">
			<?php if(!empty($icone)): ?>
			<div class="row">
				<div class="page-icone"><img alt="<?php echo get_the_title(); ?>" src="<?php echo $icone; ?>" /></div><!--
				--><div class="page-title-box">
					<h1 class="page-title"><?php echo get_the_title(); ?></h1>
					<?php if(!empty($subtitle)): ?>
					<h2 class="page-subtitle"><?php echo $subtitle; ?></h2>
					<?php endif; ?>
				</div>
			</div>
			<?php else: ?>
			<h1 class="page-title"><?php echo get_the_title(); ?></h1>
			<?php if(!empty($subtitle)): ?>
			<h2 class="page-subtitle"><?php echo $subtitle; ?></h2>
			<?php endif; ?>
			<?php endif; ?>
		</div>
	</div>

	<div id="primary" class="content-area ptl pbl">
		<div id="main" class="container inner" role="main">
			<?php if($paged == 1): ?>
			<?php $args = array('posts_per_page' => 1, 'post_type' => 'post', 'post_status' => 'publish', 'meta_key' => 'type', 'meta_value' => 'publication', 'orderby' => 'date', 'order' => 'DESC', 'suppress_filters' => false); ?>
    		<?php $posts = get_posts($args); ?>
    		<?php $post = isset($posts[0]) ? $posts[0] : null; ?>
    		<?php if(!empty($post)): ?>
    		<article class="latest-news">
    			<header class="article-header">
	    			<p class="news-date"><?php echo get_the_date('d | m | Y', $post->ID); ?></p>
	    			<hr class="separator-rouge" />
	    			<h1 class="news-title"><a href="<?php echo get_the_permalink($post->ID); ?>"><?php echo $post->post_title; ?></a></h1>
	    		</header>
	    		<div class="latest-news-body row">
	    			<div class="w70 prl news-imgs-box">
	    				<?php $news_thumbnail_id = get_post_thumbnail_id($post->ID); ?>
						<?php $news_thumbnail_url = wp_get_attachment_url( $news_thumbnail_id ); ?>
						<img class="w100" alt="<?php echo $post->post_title; ?>" src="<?php echo $news_thumbnail_url; ?>" />
	    			</div><!--
	    			--><div class="w30 news-description-box">
	    				<div class="news-description">
							<?php $resume = get_field('resume', $post->ID); ?>
							<?php if(!empty($resume)): ?>
							<?php echo excerpt_content(55, apply_filters('the_content', $resume)); ?>
							<?php else: ?>
							<?php echo excerpt_content(55, apply_filters('the_content', $post->post_content)); ?>
							<?php endif; ?>
						</div>
						<p class="txtright">
							<a class="news-link" href="<?php echo get_the_permalink($post->ID); ?>"><?php _e('Lire la suite', 'vatier'); ?><img class="mls" alt="Plus" src="<?php echo get_template_directory_uri(); ?>/images/readmore.png" /></a>
						</p>
	    			</div>
	    		</div>
    		</article>
    		<?php endif; ?>
			<?php endif; ?>

			<div class="news-grid">
				<?php $show_posts = '6'; ?>
				<?php $args = array('posts_per_page' => $show_posts, 'paged' => $paged, 'offset' => ($show_posts * ($paged - 1)) + 1, 'post_type' => 'post', 'post_status' => 'publish', 'meta_key' => 'type', 'meta_value' => 'publication', 'orderby' => 'date', 'order' => 'DESC', 'suppress_filters' => false); ?>
    			<?php $query = new WP_Query($args); ?>
    			<?php $cpt = 1; ?>
    
    			<?php while($query->have_posts()): ?>

				<?php $query->the_post(); ?>

    			<?php if($cpt % 3 == 1): ?>
				<div class="news-grid-3 grid-3">    			
    			<?php endif; ?>

    			<article class="news-item">
    				<header class="article-header">
		    			<p class="news-date"><?php echo get_the_date('d | m | Y'); ?></p>
		    			<hr class="separator-rouge" />
		    			<h2 class="news-title"><a href="<?php echo get_the_permalink(); ?>"><?php echo get_the_title(); ?></a></h2>
	    			</header>

	    			<?php $news_thumbnail_id = get_post_thumbnail_id(); ?>
					<?php $news_thumbnail_url = wp_get_attachment_url( $news_thumbnail_id ); ?>
					<div class="news-img-box"><img class="w100 bl" alt="<?php echo get_the_title(); ?>" src="<?php echo $news_thumbnail_url; ?>" /></div>
					<div class="actualite-body">
						<?php $resume = get_field('resume'); ?>
						<?php if(!empty($resume)): ?>
						<?php echo excerpt_content(55, apply_filters('the_content', $resume)); ?>
						<?php else: ?>
						<?php echo excerpt_content(55, apply_filters('the_content', get_the_content())); ?>
						<?php endif; ?>
					</div>
					<p class="txtright">
						<a class="actualite-link" href="<?php echo get_the_permalink(); ?>"><?php _e('Lire la suite', 'vatier'); ?><img class="mls" alt="Plus" src="<?php echo get_template_directory_uri(); ?>/images/readmore.png" /></a>
					</p>
    			</article>

    			<?php if($cpt % 3 == 0 || $cpt == $query->post_count): ?>
    			</div>
    			<hr />
    			<?php endif; ?>

    			<?php $cpt++; ?>
    			<?php endwhile; ?>

    			<?php $pages = $query->max_num_pages; ?>

				<?php if($pages > 1) :?>

				<div class="bl txtcenter">

					<?php if($paged > 1): ?>
					<a href="<?php echo get_the_permalink(16); ?>/page/<?php echo $paged-1; ?>" class="pagination prev"><img class="mrs" alt="Prev" src="<?php echo get_template_directory_uri(); ?>/images/prev-gray.png" /><?php _e('Précédent', 'vatier'); ?></a>
					<?php else: ?>
					<span class="pagination prev"><img class="mrs" alt="Prev" src="<?php echo get_template_directory_uri(); ?>/images/prev-gray.png" /><?php _e('Précédent', 'vatier'); ?></span>
					<?php endif; ?>

					<?php for($i=1; $i<=$pages; $i++): ?>

					<?php if($i == $paged): ?>

					<a class="pagination active">
						<img class="mbs" alt="<?php echo $i; ?>" src="<?php echo get_template_directory_uri() ?>/images/pager-active.png" /><br>
						<?php echo $i; ?>
					</a>

					<?php else: ?>

					<a href="<?php echo get_the_permalink(16); ?>/page/<?php echo $i; ?>" class="pagination">
						<img class="mbs" alt="<?php echo $i; ?>" src="<?php echo get_template_directory_uri() ?>/images/pager.png" /><br>
						<?php echo $i; ?>
					</a>

					<?php endif; ?>

					<?php endfor; ?>

					<?php if($paged < $pages): ?>
					<a href="<?php echo get_the_permalink(16); ?>/page/<?php echo $paged+1; ?>" class="pagination next"><?php _e('Suivant', 'vatier'); ?> <img class="mls" alt="Next" src="<?php echo get_template_directory_uri(); ?>/images/next-gray.png" /></a>
					<?php else: ?>
					<span class="pagination next"><?php _e('Suivant', 'vatier'); ?><img class="mls" alt="Next" src="<?php echo get_template_directory_uri(); ?>/images/next-gray.png" /></span>
					<?php endif; ?>

				</div>

				<?php endif; ?>

    			<?php wp_reset_query(); ?>
			</div>
		</div>
	</div>
<?php endwhile; ?>

<script type="text/javascript">
jQuery(document).ready(function(){
	jQuery(".section-top-image").backstretch("<?php echo $post_thumbnail_url; ?>");
});
</script>

<?php get_footer(); ?>
