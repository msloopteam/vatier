<?php
/**
 * Template Name: Template Agenda
 *
 *
 * @package vatier
 */

get_header(); ?>
<?php while ( have_posts() ) : the_post(); ?>

	<?php $post_thumbnail_id = get_post_thumbnail_id(); ?>
	<?php $post_thumbnail_url = wp_get_attachment_url( $post_thumbnail_id ); ?>
	<?php if(empty($post_thumbnail_url)): ?>
	<?php $post_thumbnail_url = get_template_directory_uri().'/images/contact.jpg'; ?>
	<?php endif; ?>
	<?php $subtitle = get_field('sous_titre'); ?>
	<?php $icone = get_field('icone'); ?>

	<div class="section-top-image">
		<div class="container inner">
			<?php if(!empty($icone)): ?>
			<div class="row">
				<div class="page-icone"><img alt="<?php echo get_the_title(); ?>" src="<?php echo $icone; ?>" /></div><!--
				--><div class="page-title-box">
					<h1 class="page-title"><?php echo get_the_title(); ?></h1>
					<?php if(!empty($subtitle)): ?>
					<h2 class="page-subtitle"><?php echo $subtitle; ?></h2>
					<?php endif; ?>
				</div>
			</div>
			<?php else: ?>
			<h1 class="page-title"><?php echo get_the_title(); ?></h1>
			<?php if(!empty($subtitle)): ?>
			<h2 class="page-subtitle"><?php echo $subtitle; ?></h2>
			<?php endif; ?>
			<?php endif; ?>
		</div>
	</div>

	<?php $month_arr = array('01' => 'Janvier', '02' => 'Février', '03' => 'Mars', '04' => 'Avril', '05' => 'Mai', '06' => 'Juin', '07' => 'Juillet', '08' => 'Aout', '09' => 'Septembre', '10' => 'Octobre', '11' => 'Novembre', '12' => 'Décembre'); ?>
	<?php $year_arr = array(); ?>
	<?php $args = array('posts_per_page' => '-1', 'post_type' => 'evenement', 'post_status' => 'publish', 'meta_key' => 'evenement_date', 'orderby' => 'meta_value_num', 'order' => 'ASC', 'suppress_filters' => false); ?>
	<?php $posts_ordred = array(); ?>
	<?php $posts = get_posts($args); ?>
	<?php foreach ($posts as $post): ?>
	<?php $date = get_field('evenement_date', $post->ID); ?>
	<?php $posts_ordred[$date][] = $post; ?>
	<?php list($year, $month, $day) =explode("-",$date); ?>
	<?php if(!in_array($year, $year_arr)) $year_arr[] = $year; ?>
	<?php $year_arr = array_unique($year_arr); ?>
	<?php endforeach; ?>
	<?php ksort($posts_ordred); ?>
	<div id="primary" class="content-area ptl pbl">
		<div id="main" class="container inner" role="main">
			<div class="filtrage-date-box">
				<div id="tabs">
				 	<ul class="filtrage-years clearfix">
				 		<?php foreach ($year_arr as $year): ?>
						<li class="filtrage-year-item"><a class="filtrage-year-btn" data-filter=".<?php echo $year; ?>" href="#tabs-<?php echo $year; ?>"><?php echo $year; ?></a></li>
						<?php endforeach; ?>
				  	</ul>

				  	<?php foreach ($year_arr as $year): ?>
				  	<div id="tabs-<?php echo $year; ?>">
				    	<ul class="filtrage-months clearfix">
							<?php foreach ($month_arr as $key => $month): ?>
							<li class="filtrage-month-item"><a class="filtrage-month-btn" data-filter=".<?php echo $year.'-'.$key; ?>"><?php echo $month; ?></a></li>
							<?php endforeach; ?>
						</ul>
				  	</div>
				  	<?php endforeach; ?>
				</div>
			</div>
			<hr />

			<div id="evenement-grid" class="clearfix">
				<?php $cpt = 1; ?>
				<?php foreach ($posts_ordred as $post_arr): ?>
				<?php foreach ($post_arr as $post): ?>
				<?php $date = get_field('evenement_date', $post->ID); ?>
				<?php list($year, $month, $day) =explode("-",$date); ?>
				<div class="evenement-item w33 fl <?php echo $year; ?> <?php echo $year.'-'.$month; ?> <?php echo 'col-'.$cpt; ?>">
					<article class="news-item">
	    				<header class="article-header">
			    			<p class="news-date"><?php echo $date; ?></p>
			    			<hr class="separator-rouge" />
			    			<h2 class="news-title"><a href="<?php echo get_the_permalink($post->ID); ?>"><?php echo $post->post_title; ?></a></h2>
		    			</header>

		    			<?php $news_thumbnail_id = get_post_thumbnail_id($post->ID); ?>
						<?php $news_thumbnail_url = wp_get_attachment_url( $news_thumbnail_id ); ?>
						<div class="news-img-box"><img class="w100 bl" alt="<?php echo $post->post_title; ?>" src="<?php echo $news_thumbnail_url; ?>" /></div>
						<div class="actualite-body">
							<?php $resume = get_field('resume', $post->ID); ?>
							<?php if(!empty($resume)): ?>
							<?php echo excerpt_content(55, apply_filters('the_content', $resume)); ?>
							<?php else: ?>
							<?php echo excerpt_content(55, apply_filters('the_content', $post->post_content)); ?>
							<?php endif; ?>
						</div>
						<p class="txtright">
							<a class="actualite-link" href="<?php echo get_the_permalink($post->ID); ?>"><?php _e('Lire la suite', 'vatier'); ?><img class="mls" alt="Plus" src="<?php echo get_template_directory_uri(); ?>/images/readmore.png" /></a>
						</p>
	    			</article>
				</div>
				<?php $cpt++; ?>
				<?php if($cpt >= 4) $cpt = 1; ?>
				<?php endforeach; ?>
				<?php endforeach; ?>
			</div>

			<div class="filtrage-date-box-bottom ptl">
				<div id="tabs-bottom">
				 	<ul class="filtrage-years clearfix">
				 		<?php foreach ($year_arr as $year): ?>
						<li class="filtrage-year-item"><a class="filtrage-year-btn" data-filter=".<?php echo $year; ?>" href="#tabs-bottom-<?php echo $year; ?>"><?php echo $year; ?></a></li>
						<?php $index++; ?>
						<?php endforeach; ?>
				  	</ul>

				  	<?php foreach ($year_arr as $year): ?>
				  	<div id="tabs-bottom-<?php echo $year; ?>">
				    	<ul class="filtrage-months clearfix">
							<?php foreach ($month_arr as $key => $month): ?>
							<li class="filtrage-month-item"><a class="filtrage-month-btn" data-filter=".<?php echo $year.'-'.$key; ?>"><?php echo $month; ?></a></li>
							<?php endforeach; ?>
						</ul>
				  	</div>
				  	<?php endforeach; ?>
				</div>
			</div>
			<?php wp_reset_postdata(); ?>
		</div>
	</div>
<?php endwhile; ?>

<script type="text/javascript">
jQuery(document).ready(function(){
	jQuery(".section-top-image").backstretch("<?php echo $post_thumbnail_url; ?>");

	var index = <?php echo array_search(date("Y"), $year_arr); ?>

	jQuery('#tabs').tabs({
		active: index,
	});

	jQuery('#tabs-bottom').tabs({
		active: index,
	});
});

jQuery(window).load(function(){
	var grid = jQuery('#evenement-grid').isotope({
		percentPosition: true,
		layoutMode: 'fitRows',
		itemSelector: '.evenement-item',
	});

	jQuery('.filtrage-month-btn').on( 'click', function() {
	  	var filterValue = jQuery(this).attr('data-filter');
	  	jQuery('.filtrage-month-btn').removeClass('active');
	  	var index_li = jQuery(this).parent('li').index();
	  	jQuery('.filtrage-months').each(function(){
	  		jQuery('li', this).eq(index_li).find('.filtrage-month-btn').addClass('active');
	  	});
	  	grid.isotope({ filter: filterValue });
	});

	jQuery('.filtrage-year-btn').on( 'click', function() {
	  	var filterValue = jQuery(this).attr('data-filter');
	  	jQuery('.filtrage-years > li').removeClass('ui-tabs-active');
	  	jQuery('.filtrage-month-btn').removeClass('active');
	  	var index_li = jQuery(this).parent('li').index();
	  	jQuery('.filtrage-years').each(function(){
	  		jQuery('li', this).eq(index_li).addClass('ui-tabs-active');
	  	});
	  	grid.isotope({ filter: filterValue });
	});

	grid.isotope({ filter: '.<?php echo date("Y"); ?>' });

});
</script>

<?php get_footer(); ?>
