<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package vatier
 */

get_header(); ?>
	<ul class="flex-sociaux-block">
		<li><a id="facebook" title="Facebook" href="#" target="_blank"><img alt="Facebook" src="<?php echo get_template_directory_uri(); ?>/images/facebook.png" /></a></li>
		<li><a id="twitter" title="Twitter" href="#" target="_blank"><img alt="Twitter" src="<?php echo get_template_directory_uri(); ?>/images/twitter.png" /></a></li>
		<li><a id="linkedin" title="LinkedIn" href="#" target="_blank"><img alt="LinkedIn" src="<?php echo get_template_directory_uri(); ?>/images/linkedin.png" /></a></li>
	</ul>

	<div class="slides-section clearfix">
		<?php if(ICL_LANGUAGE_CODE == "fr"): ?>
		<?php echo do_shortcode("[metaslider id=6]"); ?>
		<?php else: ?>
		<?php echo do_shortcode("[metaslider id=241]"); ?>
		<?php endif; ?>
	</div>

	<div class="section-white">
		<div class="container inner">
			<h2 class="section-title clearfix">
				<img class="verti-middle" alt="<?php _e('Offre', 'vatier'); ?>" src="<?php echo get_template_directory_uri(); ?>/images/logo.png" /><span class="txt-rouge upper mls verti-middle"><?php _e('Offre', 'vatier'); ?></span>
				<a class="fr all-link" href="<?php echo get_the_permalink(12); ?>"><?php _e('Accéder à toutes les offres', 'vatier'); ?><img class="mls" alt="<?php _e('Accéder à toutes les offres', 'vatier'); ?>" src="<?php echo get_template_directory_uri(); ?>/images/goto.png" /></a>
			</h2>

			<div id="offres-grid" class="grid grid-4 mtl mbl">
			<?php $offres = get_home_offres(); ?>
			<?php foreach ($offres as $offre): ?>
				<?php $post_thumbnail_id = get_post_thumbnail_id($offre->ID); ?>
				<?php $post_thumbnail_url = wp_get_attachment_url( $post_thumbnail_id ); ?>
				<div class="offre-item txtcenter">
					<a class="bl offre-link" href="<?php echo get_the_permalink($offre->ID); ?>">
					<p class="offre-img"><img alt="<?php echo $offre->post_title; ?>" src="<?php echo $post_thumbnail_url; ?>" /></p>
					<p class="offre-title"><?php echo $offre->post_title; ?></p>
					</a>
				</div>
			<?php endforeach; ?>
			</div>
		</div>
	</div>

	<div class="section-gray">
		<div class="container inner">
			<h2 class="section-title clearfix">
				<img class="verti-middle" alt="<?php _e('Actus', 'vatier'); ?>" src="<?php echo get_template_directory_uri(); ?>/images/logo.png" /><span class="txt-rouge upper mls verti-middle"><?php _e('Actus', 'vatier'); ?></span>
				<a class="fr all-link" href="<?php echo get_the_permalink(14); ?>"><?php _e('Accéder aux autres actus', 'vatier'); ?><img class="mls" alt="<?php _e('Accéder aux autres actus', 'vatier'); ?>" src="<?php echo get_template_directory_uri(); ?>/images/goto.png" /></a>
			</h2>

			<div id="actualites-grid" class="mtl mbl clearfix">
			<?php $actualites = get_home_actualites(); ?>
			<?php $index = 1; ?>
			<?php foreach ($actualites as $actualite): ?>
				<?php $post_thumbnail_id = get_post_thumbnail_id($actualite->ID); ?>
				<?php $post_thumbnail_url = wp_get_attachment_url( $post_thumbnail_id ); ?>
				<div class="actualite-item<?php if($index==2) echo " middle-item"; ?>">
					<p class="actualite-date"><?php echo get_the_date('d | m | Y', $actualite->ID); ?></p>
					<hr class="separator-rouge"/>
					<h3 class="actualite-title txt-rouge"><a href="<?php echo get_the_permalink($actualite->ID); ?>"><?php echo wp_trim_words($actualite->post_title, 10, ' ...'); ?></a></h3>
					<p class="actualite-img"><img class="w100 bl" alt="<?php echo $actualite->post_title; ?>" src="<?php echo $post_thumbnail_url; ?>" /></p>
					<div class="actualite-body">
						<?php $resume = get_field('resume', $actualite->ID); ?>
						<?php if(!empty($resume)): ?>
						<?php echo excerpt_content(55, apply_filters('the_content', $resume)); ?>
						<?php else: ?>
						<?php echo excerpt_content(55, apply_filters('the_content', $actualite->post_content)); ?>
						<?php endif; ?>
					</div>
					<p class="txtright">
						<a class="actualite-link" href="<?php echo get_the_permalink($actualite->ID); ?>"><?php _e('Lire la suite', 'vatier'); ?><img class="mls" alt="Plus" src="<?php echo get_template_directory_uri(); ?>/images/readmore.png" /></a>
					</p>
				</div>
				<?php $index++; ?>
			<?php endforeach; ?>
			</div>
		</div>
	</div>

	<div class="section-gradient">
		<div class="container inner">
			<?php $equipes = get_home_equipe(); ?>
			<?php $equipe = $equipes[0]; ?>
			<?php $photo_homepage = get_field('photo_homepage', $equipe->ID); ?>
			<div class="row equipe-box">
				<div class="w50 prm equipe-photo-home">
					<img alt="<?php echo $equipe->post_title; ?>" src="<?php echo $photo_homepage; ?>" />
				</div><!--
				--><div class="w50 plm equipe-content">
					<h2 class="section-title no-border clearfix">
						<img class="verti-middle" alt="<?php _e('Focus avocat', 'vatier'); ?>" src="<?php echo get_template_directory_uri(); ?>/images/logo.png" /><span class="txt-rouge upper mls verti-middle"><?php _e('Focus avocat', 'vatier'); ?></span>
					</h2>
					<h3 class="equipe-name"><?php echo $equipe->post_title; ?></h3>
					<hr class="separator-rouge"/>
					<div class="equipe-description mtm">
						<?php echo excerpt_content(70, apply_filters('the_content', get_field('description', $equipe->ID))); ?>
					</div>
					<div class="equipe-intervention mtm">
						<p class="txt-rouge mbn"><strong>Principaux domaines d’intervention</strong></p>
						<?php echo excerpt_content(70, apply_filters('the_content', get_field('domaines_intervention', $equipe->ID))); ?>
					</div>
					<div class="equipe-more-link">
						<a class="all-link upper" href="<?php echo get_the_permalink(10); ?>"><?php _e('Voir toute l’équipe', 'vatier'); ?><img class="mls" alt="<?php _e('Voir toute l’équipe', 'vatier'); ?>" src="<?php echo get_template_directory_uri(); ?>/images/goto.png" /></a>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="section-black txt-white">
		<div class="container inner">
			<h2 class="section-wihte-title clearfix">
				<img class="verti-middle" alt="<?php _e('Contact', 'vatier'); ?>" src="<?php echo get_template_directory_uri(); ?>/images/logo-white.png" /><span class="txt-white upper mls verti-middle"><?php _e('Contact', 'vatier'); ?></span>
			</h2>
			<div class="row mtm">
				<div class="w30 home-contact-info">
					<?php $contact_paris = get_option('contact_paris'); ?>
					<?php if(!empty($contact_paris)): ?>
					<div class="clearfix home-contact-location">
						<span class="contact-title upper"><?php _e('Vatier Paris', 'vatier'); ?></span><br>
						<div class="contact-infos">
							<?php echo nl2br(do_shortcode($contact_paris)); ?>
						</div>
					</div>
					<?php endif; ?>

					<?php $contact_bruxelles = get_option('contact_bruxelles'); ?>
					<?php if(!empty($contact_bruxelles)): ?>
					<hr class="separator-white" />
					<div class="clearfix home-contact-location">
						<span class="contact-title upper"><?php _e('Vatier Bruxelles', 'vatier'); ?></span><br>
						<div class="contact-infos">
							<?php echo nl2br(do_shortcode($contact_bruxelles)); ?>
						</div>
					</div>
					<?php endif; ?>

				</div><!--
				--><div class="w70 pll home-contact-map">
					<div id="map-tabs" class="clearfix">
						<span class="upper label">Afficher le plan de</span>
						<?php $map_latitude_paris = get_option('map_latitude_paris'); ?>
						<?php $map_longitude_paris = get_option('map_longitude_paris'); ?>
						<?php $map_latitude_bruxelles = get_option('map_latitude_bruxelles'); ?>
						<?php $map_longitude_bruxelles = get_option('map_longitude_bruxelles'); ?>
						<ul>
							<?php if(!empty($map_latitude_paris) && !empty($map_longitude_paris)): ?>
							<li><a class="tabs-link upper" href="#tabs-1"><?php _e('Paris', 'vatier'); ?></a></li>
							<?php endif; ?>
							<?php if(!empty($map_latitude_bruxelles) && !empty($map_longitude_bruxelles)): ?>
							<li><a class="tabs-link upper" href="#tabs-2"><?php _e('Bruxelles', 'vatier'); ?></a></li>
							<?php endif; ?>
						</ul>
						<?php if(!empty($map_latitude_paris) && !empty($map_longitude_paris)): ?>
						<div id="tabs-1" class="tabs-box">
							<div id="map-paris" class="map-canvas"></div>
						</div>
						<?php endif; ?>
						<?php if(!empty($map_latitude_bruxelles) && !empty($map_longitude_bruxelles)): ?>
						<div id="tabs-2" class="tabs-box">
							<div id="map-bruxelles" class="map-canvas"></div>
						</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
<script type="text/javascript">
	jQuery(window).load(function(){
		initialize();
		jQuery('#map-tabs').tabs({
			activate: function( event, ui ){
				initialize();
			}
		});
	});

	function initialize() {
		<?php if(!empty($map_latitude_paris) && !empty($map_longitude_paris)): ?>
		
		var location = new google.maps.LatLng(<?php echo $map_latitude_paris ?>, <?php echo $map_longitude_paris; ?>);

		var map_paris = new google.maps.Map(document.getElementById("map-paris"), {
				        zoom: 15,
				        scrollwheel: false,
				        streetViewControl: false,
				        //disableDefaultUI: true,
				        center: location,
				        mapTypeId: google.maps.MapTypeId.ROADMAP,
			      	});
		var marker = new MarkerWithLabel({
			            map: map_paris,
			            position: location,
			            animation: google.maps.Animation.DROP,
			            title: 'Vatier Paris',
			            labelAnchor: new google.maps.Point(<?php echo $map_latitude_paris ?>, <?php echo $map_longitude_paris; ?>),
			            labelClass: "google-labels", 
			            labelStyle: {opacity: 0.75}
		          	});
		
		google.maps.event.addListener(map_paris, 'tilesloaded', function() {
        	jQuery(".google-labels").text("Vatier Paris");
      	});

		<?php endif; ?>

		<?php if(!empty($map_latitude_bruxelles) && !empty($map_longitude_bruxelles)): ?>
		
		var location = new google.maps.LatLng(<?php echo $map_latitude_bruxelles ?>, <?php echo $map_longitude_bruxelles; ?>);

		var map_bruxelles = new google.maps.Map(document.getElementById("map-bruxelles"), {
				        zoom: 15,
				        scrollwheel: false,
				        streetViewControl: false,
				        //disableDefaultUI: true,
				        center: location,
				        mapTypeId: google.maps.MapTypeId.ROADMAP,
			      	});
		var marker = new MarkerWithLabel({
			            map: map_bruxelles,
			            position: location,
			            animation: google.maps.Animation.DROP,
			            title: 'Vatier Bruxelles',
			            labelAnchor: new google.maps.Point(<?php echo $map_latitude_paris ?>, <?php echo $map_longitude_paris; ?>),
			            labelClass: "google-labels", 
			            labelStyle: {opacity: 0.75}
		          	});
		
		google.maps.event.addListener(map_bruxelles, 'tilesloaded', function() {
        	jQuery(".google-labels").text("Vatier Bruxelles");
      	});

		<?php endif; ?> 
	} 
</script>
<?php get_footer(); ?>
