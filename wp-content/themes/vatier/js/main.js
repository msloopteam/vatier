'use strict'

jQuery(document).ready(function(){
	
});

jQuery(window).load(function(){
	jQuery('.offre-item').matchHeight({byRow: true});
	jQuery('.cabinet-bloc').matchHeight({byRow: true});
	jQuery('.actualite-img').matchHeight({byRow: true});
	jQuery('.news-img-box').matchHeight({byRow: true});

	jQuery('#mobile-article-select').on('change', function(event){
		var url = jQuery(this).val();
		if(url != "")
		{
			document.location.href = url;
		}
	});
});