<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package vatier
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico" />
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>
<!--[if lt IE 9]>
    <script src="<?php echo get_template_directory_uri(); ?>/js/html5shiv.js "></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/respond.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/jquery.placeholder.js" ></script>
    <script>
	jQuery(document).ready(function(){
		jQuery('input, textarea').placeholder();
	});
    </script>
<![endif]-->
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'vatier' ); ?></a>

	<header id="header" class="site-header" role="banner">
		<nav id="navigation" class="main-navigation container inner" role="navigation">
			<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu', 'menu_class' => 'clearfix', 'container_class' => 'clearfix' ) ); ?>
		</nav>
	</header>

	<div id="content" class="site-content">