<?php
/**
 * Custom functions that act independently of the theme templates.
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package vatier
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function vatier_body_classes( $classes ) {
	// Adds a class of group-blog to blogs with more than 1 published author.
	if ( is_multi_author() ) {
		$classes[] = 'group-blog';
	}

	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	return $classes;
}
add_filter( 'body_class', 'vatier_body_classes' );

/**
* Remove wp generator
*/
automatic_feed_links(false);
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0 );
remove_action('wp_head', 'wp_dlmp_l10n_style' );
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wlwmanifest_link');


/***************************************/
/*									   */
/* Remove all widgets     			   */
/*									   */
/***************************************/
function remove_default_widgets() {
    unregister_widget('WP_Widget_Pages');
    unregister_widget('WP_Widget_Calendar');
    unregister_widget('WP_Widget_Archives');
    unregister_widget('WP_Widget_Links');
    unregister_widget('WP_Widget_Meta');
    unregister_widget('WP_Widget_Search');
    unregister_widget('WP_Widget_Text');
    unregister_widget('WP_Widget_Categories');
    unregister_widget('WP_Widget_Recent_Posts');
    unregister_widget('WP_Widget_Recent_Comments');
    unregister_widget('WP_Widget_RSS');
    unregister_widget('WP_Widget_Tag_Cloud');
    unregister_widget('WP_Nav_Menu_Widget');
}
add_action('widgets_init', 'remove_default_widgets', 11);

/***************************************/
/*                                     */
/* Rewrite Search URL                  */
/*                                     */
/***************************************/

function vatier_change_search_url_rewrite() {
    if ( is_search() && ! empty( $_GET['s'] ) ) {
        wp_redirect( home_url( "/search/" ) . urlencode( get_query_var( 's' ) ) );
        exit();
    }   
}
add_action( 'template_redirect', 'vatier_change_search_url_rewrite' );


/********************************************/
/*                                          */
/* Add font-size and font-family in editor  */
/*                                          */
/********************************************/
function wpex_mce_text_sizes($initArray){
   $initArray['fontsize_formats'] = "9px 10px 12px 13px 14px 16px 18px 20px 22px 24px 26px 28px 30px 32px 36px";
   $initArray['font_formats'] = 'Sansation Regular=sansationregular;Open Sansation Light=sansationlight;Sansation Bold=sansationbold;';
   return $initArray;
}

add_filter('tiny_mce_before_init', 'wpex_mce_text_sizes');

/**************************************************/
/*                                                */
/* Add button font-size and font-family in editor */
/*                                                */
/**************************************************/
function wpex_mce_add_more_buttons($buttons){
    $buttons[] = 'fontselect';
    $buttons[] = 'fontsizeselect';
    return $buttons;
}

add_filter("mce_buttons_3", "wpex_mce_add_more_buttons");

/********************************/
/*                              */
/* Custom excerpt               */
/*                              */
/********************************/
function excerpt($limit) {
    $excerpt = explode(' ', get_the_excerpt(), $limit);
    if (count($excerpt)>=$limit) {
        array_pop($excerpt);
        $excerpt = implode(" ",$excerpt).'...';
    } else {
        $excerpt = implode(" ",$excerpt);
    } 
    $excerpt = preg_replace('`\[[^\]]*\]`','',$excerpt);
    return $excerpt;
}

function excerpt_content($limit, $content=null) {
    $excerpt = explode(' ', $content, $limit);
    if (count($excerpt)>=$limit) {
        array_pop($excerpt);
        $excerpt = implode(" ",$excerpt).'...';
    } else {
        $excerpt = implode(" ",$excerpt);
    } 
    $excerpt = preg_replace('`\[[^\]]*\]`','',$excerpt);
    return $excerpt;
}

/**************************************************/
/*												  */
/* Anti spam --- email 							  */
/*												  */
/**************************************************/
function email_encode($atts, $email){
    $params = shortcode_atts(array(
        'title' => '',
    ), $atts);

    if($params['title'] == '')
    {
        $params['title'] = antispambot($email);
    }

	return '<a href="mailto:'.antispambot($email).'">'.$params['title'].'</a>';
}
add_shortcode( 'email', 'email_encode' );

/**********************************************/
/*                                            */
/* Set parent page of post                    */
/*                                            */
/**********************************************/
function add_class_to_wp_nav_menu($classes)
{

    if(is_single())
    {
        switch (get_post_type())
        {
            case 'post':
                $type = get_field('type');

                if($type == "actualite" && in_array('menu-item-34', $classes))
                {
                   $classes[] = 'current-menu-parent';
                }
                elseif($type == "publication" && in_array('menu-item-36', $classes))
                {
                    $classes[] = 'current-menu-parent';
                }
                break;
        }
    }
    return $classes;
}
add_filter('nav_menu_css_class', 'add_class_to_wp_nav_menu');


function translate_date_format($format) {
    if (function_exists('icl_translate'))
      $format = icl_translate('Formats', $format, $format);
return $format;
}
add_filter('option_date_format', 'translate_date_format');

function language_selector_flags(){
    $languages = icl_get_languages('skip_missing=0&orderby=code');
    $html = '';
    if(!empty($languages)){

        foreach($languages as $l){
            $html .= '<a class="flags-link" href="'.$l['url'].'">';
            if($l['active'])
            {
                $html .=  '<span class="current language-code">'.$l['language_code'].'</span>';
            }
            else
            {
                $html .=  '<span class="language-code">'.$l['language_code'].'</span>';
            }
            $html .=  '</a>';
        }
    }

    return $html;
}


/**************************************************/
/*                                                */
/* Add menu language select                       */
/*                                                */
/**************************************************/

function primary_nav_menu_items($items, $args) {
    if( $args->menu_id == 'primary-menu' )
    {
        $languagelink = '<li class="menu-item language-select">'.language_selector_flags().'</li>';
        $items = $items . $languagelink;
    }
    return $items;
}
add_filter( 'wp_nav_menu_items', 'primary_nav_menu_items', 10, 2 );

/**************************************************/
/*                                                */
/* Theme settings                                 */
/*                                                */
/**************************************************/

add_action('admin_init', 'vatierRegisterSettings');

function vatierRegisterSettings(){
    register_setting('vatier', 'contact_paris');
    register_setting('vatier', 'map_latitude_paris');
    register_setting('vatier', 'map_longitude_paris');

    register_setting('vatier', 'contact_bruxelles');
    register_setting('vatier', 'map_latitude_bruxelles');
    register_setting('vatier', 'map_longitude_bruxelles');
}

add_action('admin_menu', 'vatierAdminMenu');

function vatierAdminMenu(){
    add_submenu_page('themes.php', 'Options', 'Options', 'manage_options', 'theme-options', 'vatierSettingsPage');
}

function vatierSettingsPage(){
    if(isset($_POST["update_settings"])){
        $contact_paris = $_POST["contact_paris"];
        $map_latitude_paris = $_POST['map_latitude_paris'];
        $map_longitude_paris = $_POST['map_longitude_paris'];

        $contact_bruxelles = $_POST['contact_bruxelles'];
        $map_latitude_bruxelles = $_POST['map_latitude_bruxelles'];
        $map_longitude_bruxelles = $_POST['map_longitude_bruxelles'];

        update_option("contact_paris", $contact_paris);
        update_option("map_latitude_paris", $map_latitude_paris);
        update_option("map_longitude_paris", $map_longitude_paris);

        update_option("contact_bruxelles", $contact_bruxelles);
        update_option("map_latitude_bruxelles", $map_latitude_bruxelles);
        update_option("map_longitude_bruxelles", $map_longitude_bruxelles);
        ?>
        <div id="message" class="updated"><?php _e('Settings saved', 'vatier'); ?></div>
        <?php
    }
    ?>
    <div class="wrap">
        <h2><?php _e('Theme options', 'vatier'); ?></h2>

        <form method="post" action="">
            <?php settings_fields('vatier_settings'); ?>
            <table class="form-table">
                <tr valign="top"><td><h3><?php _e('Contact Paris', 'vatier'); ?></h3></td></tr>
                <tr valign="top">
                    <td>
                        <?php wp_editor(stripslashes_deep(get_option('contact_paris')), 'contact_paris', array('media_buttons' => false, 'textarea_name' => 'contact_paris', 'textarea_rows' => '8', 'teeny' => true)); ?>
                    </td>
                </tr>
                <tr valign="top">
                    <td><?php _e('Latitude :', 'vatier'); ?> <input class="regular-text ltr" type="text" id="map_latitude_paris" name="map_latitude_paris" value="<?php echo stripslashes_deep(get_option('map_latitude_paris')); ?>" /></td>
                </tr>

                <tr valign="top">
                    <td><?php _e('Longitude :', 'vatier'); ?> <input class="regular-text ltr" type="text" id="map_longitude_paris" name="map_longitude_paris" value="<?php echo stripslashes_deep(get_option('map_longitude_paris')); ?>" /></td>
                </tr>

                <tr valign="top"><td><br><br><h3><?php _e('Contact Bruxelles', 'vatier'); ?></h3></td></tr>
                <tr valign="top">
                    <td>
                        <?php wp_editor(stripslashes_deep(get_option('contact_bruxelles')), 'contact_bruxelles', array('media_buttons' => false, 'textarea_name' => 'contact_bruxelles', 'textarea_rows' => '8', 'teeny' => true)); ?>
                    </td>
                </tr>
                <tr valign="top">
                    <td><?php _e('Latitude :', 'vatier'); ?> <input class="regular-text ltr" type="text" id="map_latitude_bruxelles" name="map_latitude_bruxelles" value="<?php echo stripslashes_deep(get_option('map_latitude_bruxelles')); ?>" /></td>
                </tr>

                <tr valign="top">
                    <td><?php _e('Longitude :', 'vatier'); ?> <input class="regular-text ltr" type="text" id="map_longitude_bruxelles" name="map_longitude_bruxelles" value="<?php echo stripslashes_deep(get_option('map_longitude_bruxelles')); ?>" /></td>
                </tr>
            </table>
            <p class="submit">
                <input name="update_settings" type="submit" class="button-primary" value="<?php _e('Update', 'vatier'); ?>" />
            </p>
        </form>
    </div>
    <?php
}

/**************************************************/
/*                                                */
/* Get home offres                                */
/*                                                */
/**************************************************/

function get_home_offres(){
    $args = array('posts_per_page' => -1, 'post_type' => 'offre_bloc', 'post_status' => 'publish', 'meta_key' => 'home_page', 'meta_value' => true, 'orderby' => 'date', 'order' => 'ASC', 'suppress_filters' => false);
    $posts = get_posts($args);
    return $posts;
}

/**************************************************/
/*                                                */
/* Get home actualités                            */
/*                                                */
/**************************************************/

function get_home_actualites(){
    $args = array('posts_per_page' => 3, 'post_type' => 'post', 'post_status' => 'publish', 'meta_key' => 'type', 'meta_value' => 'actualite', 'orderby' => 'date', 'order' => 'DESC', 'suppress_filters' => false);
    $posts = get_posts($args);
    return $posts;
}

/**************************************************/
/*                                                */
/* Get home équipe                                */
/*                                                */
/**************************************************/

function get_home_equipe(){
    $args = array('posts_per_page' => 1, 'post_type' => 'equipe', 'post_status' => 'publish', 'meta_key' => 'home_page', 'meta_value' => true, 'orderby' => 'date', 'order' => 'DESC', 'suppress_filters' => false);
    $posts = get_posts($args);
    return $posts;
}

/**************************************************/
/*                                                */
/* Ajax pagination                                */
/*                                                */
/**************************************************/

function vatier_get_article_menu(){
    $params = $_POST['param'];
    $type = $params['type'];
    $paged = $params['paged'];
    $html = '';
    
    $args = array('posts_per_page' => 10, 'paged' => $paged, 'post_type' => 'post', 'post_status' => 'publish', 'meta_key' => 'type', 'meta_value' => $type, 'orderby' => 'date', 'order' => 'DESC', 'suppress_filters' => false);
    $query = new WP_Query($args);
    while($query->have_posts())
    {
        $query->the_post();
        $html .= '<li class="news-menu-item">';
        $html .= '<p class="article-date">'.get_the_date('d | m | Y').'</p>';
        $html .= '<hr class="separator-rouge" />';
        $html .= '<p class="news-menu-link"><a href="'.get_the_permalink().'">'.get_the_title().'</a></p>';
        $html .= '</li>';
    }
    wp_reset_query();
    wp_send_json($html);
    die();
}

add_action( 'wp_ajax_vatier_get_article_menu', 'vatier_get_article_menu' );
add_action( 'wp_ajax_nopriv_vatier_get_article_menu', 'vatier_get_article_menu' );