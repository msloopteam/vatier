<?php
/**
 * Template Name: Template Cabinet
 *
 *
 * @package vatier
 */

get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

	<?php $post_thumbnail_id = get_post_thumbnail_id(); ?>
	<?php $post_thumbnail_url = wp_get_attachment_url( $post_thumbnail_id ); ?>
	<?php if(empty($post_thumbnail_url)): ?>
	<?php $post_thumbnail_url = get_template_directory_uri().'/images/contact.jpg'; ?>
	<?php endif; ?>
	<?php $subtitle = get_field('sous_titre'); ?>
	<?php $icone = get_field('icone'); ?>

	<div class="section-top-image">
		<div class="container inner">
			<?php if(!empty($icone)): ?>
			<div class="row">
				<div class="page-icone"><img alt="<?php echo get_the_title(); ?>" src="<?php echo $icone; ?>" /></div><!--
				--><div class="page-title-box">
					<h1 class="page-title"><?php echo get_the_title(); ?></h1>
					<?php if(!empty($subtitle)): ?>
					<h2 class="page-subtitle"><?php echo $subtitle; ?></h2>
					<?php endif; ?>
				</div>
			</div>
			<?php else: ?>
			<h1 class="page-title"><?php echo get_the_title(); ?></h1>
			<?php if(!empty($subtitle)): ?>
			<h2 class="page-subtitle"><?php echo $subtitle; ?></h2>
			<?php endif; ?>
			<?php endif; ?>
		</div>
	</div>

	<div class="section-gray-light">
		<div class="container inner">
			<h2 class="section-wihte-title upper txt-rouge"><img class="verti-middle" alt="<?php _e('Présentation', 'vatier'); ?>" src="<?php echo get_template_directory_uri(); ?>/images/logo.png" /><span class="mls verti-middle"><?php _e('Présentation', 'vatier'); ?></span></h2>
			<div class="page-body clearfix"><?php the_content(); ?></div>
		</div>
	</div>

	<div class="section-white">
		<div class="container inner">
			<h2 class="section-wihte-title upper txt-rouge txtcenter"><img class="verti-middle" alt="<?php _e('Nos activités', 'vatier'); ?>" src="<?php echo get_template_directory_uri(); ?>/images/logo.png" /><span class="mls verti-middle"><?php _e('Nos activités', 'vatier'); ?></span></h2>
			<div class="nos-activites-box ptm pbm">
				<div class="grid-3 activites-grid">
					<?php if(have_rows('activites')): ?>
					<?php $cpt = 0; ?>
					<?php while(have_rows('activites')): ?>
					<?php the_row(); ?>
					<?php $activite_icone = get_sub_field('activite_icone'); ?>
					<?php $activite_titre = get_sub_field('activite_titre'); ?>
					<?php $activite_description = get_sub_field('activite_description'); ?>
					<div class="activite-item txtcenter">
						<p class="activite-icone-box man"><img class="activite-icone" alt="<?php echo $activite_titre; ?>" src="<?php echo $activite_icone; ?>" /></p>
						<p class="activite-titre txt-rouge upper mtm"><strong><?php echo $activite_titre; ?></strong></p>
						<div class="activite-description mtm">
							<?php echo apply_filters('the_content', $activite_description); ?>
						</div>
					</div>
					<?php $cpt++; ?>
					<?php endwhile; ?>
					<?php if($cpt % 3 == 2): ?>
					<div class="activite-item txtcenter"></div>
					<?php endif; ?>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>

	<div class="clearfix">
		<div class="container inner">
			<div class="cabinet-blocs clearfix">
				<?php $color_arr = array('block-gray', 'block-gray-lignt', 'block-gray-lignt', 'block-black'); ?>
				<?php if(have_rows('blocs')): ?>
				<?php $cpt = 0; ?>
				<?php while(have_rows('blocs')): ?>
				<?php the_row(); ?>
				<?php $color = $color_arr[$cpt % 4]; ?>
				<?php $titre = get_sub_field('titre'); ?>
				<?php $description = get_sub_field('description'); ?>
				<div class="cabinet-bloc <?php echo $color; ?>">
					<h3 class="cabinet-bloc-title <?php echo $color; ?>-title"><?php echo $cpt+1; ?> - <?php echo $titre; ?></h3>
					<div class="cabinet-bloc-description <?php echo $color; ?>-description"><?php echo apply_filters('the_content', $description); ?></div>
				</div>
				<?php $cpt++; ?>
				<?php endwhile; ?>
				<?php endif; ?>
			</div>
		</div>
	</div>

<?php endwhile; ?>

<script type="text/javascript">
jQuery(document).ready(function(){
	jQuery(".section-top-image").backstretch("<?php echo $post_thumbnail_url; ?>");	
});
</script>

<?php get_footer(); ?>
