<?php
/**
 * Template Name: Template Équipe
 *
 *
 * @package vatier
 */

get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

	<?php $post_thumbnail_id = get_post_thumbnail_id(); ?>
	<?php $post_thumbnail_url = wp_get_attachment_url( $post_thumbnail_id ); ?>
	<?php if(empty($post_thumbnail_url)): ?>
	<?php $post_thumbnail_url = get_template_directory_uri().'/images/contact.jpg'; ?>
	<?php endif; ?>
	<?php $subtitle = get_field('sous_titre'); ?>
	<?php $icone = get_field('icone'); ?>

	<div class="section-top-image">
		<div class="container inner">
			<?php if(!empty($icone)): ?>
			<div class="row">
				<div class="page-icone"><img alt="<?php echo get_the_title(); ?>" src="<?php echo $icone; ?>" /></div><!--
				--><div class="page-title-box">
					<h1 class="page-title"><?php echo get_the_title(); ?></h1>
					<?php if(!empty($subtitle)): ?>
					<h2 class="page-subtitle"><?php echo $subtitle; ?></h2>
					<?php endif; ?>
				</div>
			</div>
			<?php else: ?>
			<h1 class="page-title"><?php echo get_the_title(); ?></h1>
			<?php if(!empty($subtitle)): ?>
			<h2 class="page-subtitle"><?php echo $subtitle; ?></h2>
			<?php endif; ?>
			<?php endif; ?>
		</div>
	</div>

	<div class="section-white">
		<div class="container inner">
			<div class="filter-btn-group clearfix txtcenter">
				<div class="filter-btn-box">
					<button class="filter-btn active" data-filter="*"><?php _e('Tous', 'vatier'); ?></button>
				</div>
				<?php $categories = get_terms( 'fonction', array('hide_empty' => 0) ); ?>
				<?php foreach ($categories as $category): ?>
				<div class="filter-btn-box">
					<button class="filter-btn" data-filter=".<?php echo $category->slug; ?>"><?php echo $category->name; ?></button>
				</div>
				<?php endforeach; ?>
			</div>
		</div>
	</div>

	<div class="content-area bg-white">
		<div id="main" class="container inner" role="main">
			<div class="equipe-gallery mbl clearfix">
			<?php $args = array('posts_per_page' => '-1', 'post_type' => 'equipe', 'post_status' => 'publish', 'orderby' => 'title', 'order' => 'ASC', 'suppress_filters' => false); ?>
		    <?php $posts = get_posts($args); ?>
		    <?php foreach ($posts as $equipe): ?>
		    	<?php $terms = get_the_terms($equipe->ID, 'fonction'); ?>
		    	<?php $fonction = ''; ?>
		    	<?php if(isset($terms[0])) $fonction = $terms[0]; ?>
		       	<?php $equipe_thumbnail_id = get_post_thumbnail_id($equipe->ID); ?>
				<?php $equipe_thumbnail_url = wp_get_attachment_url( $equipe_thumbnail_id ); ?>
				<div class="equipe-item w33 fl <?php echo isset($fonction->slug) ? $fonction->slug : ''; ?>">
					<a class="equipe-block-link" href="<?php echo get_the_permalink($equipe->ID); ?>">
					<img class="equipe-photo" alt="<?php echo $equipe->post_title; ?>" src="<?php echo $equipe_thumbnail_url; ?>" />
					<div class="equipe-info row">
						<div class="w60">
						<p class="equipe-person"><?php echo $equipe->post_title; ?></p>
						<?php if(!empty($fonction)): ?>
						<p class="equipe-fonction"><?php echo $fonction->name; ?></p>
						<?php endif; ?>
						</div><!--
						--><div class="w40 verti-middle txtright">
							<span class="euiper-more-link"><?php _e('Voir le détail', 'vatier'); ?><img class="mls" alt="Plus" src="<?php echo get_template_directory_uri(); ?>/images/readmore.png"></span> 
						</div>
					</div>
					</a>
				</div>

		    <?php endforeach; ?>
			</div>
		</div>
	</div>
<?php endwhile; ?>

<script type="text/javascript">
jQuery(document).ready(function(){
	jQuery(".section-top-image").backstretch("<?php echo $post_thumbnail_url; ?>");	
});

jQuery(window).load(function(){
	var grid = jQuery('.equipe-gallery').isotope();

	jQuery('.filter-btn').on( 'click', function() {
	  	var filterValue = jQuery(this).attr('data-filter');
	  	jQuery('.filter-btn').removeClass('active');
	  	jQuery(this).addClass('active');
	  	grid.isotope({ filter: filterValue });
	});
});
</script>

<?php get_footer(); ?>
