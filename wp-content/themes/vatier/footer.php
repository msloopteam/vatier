<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package vatier
 */

?>

	</div><!-- #content -->

	<footer id="footer" class="site-footer" role="contentinfo">
		<div id="footer-box" class="container inner">
			<?php wp_nav_menu( array( 'theme_location' => 'secondary', 'menu_id' => 'footer-menu', 'menu_class' => 'clearfix', 'container_class' => 'clearfix') ); ?>
		</div>
	</footer>
</div>

<?php wp_footer(); ?>

</body>
</html>
