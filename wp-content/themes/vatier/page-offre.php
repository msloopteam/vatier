<?php
/**
 * Template Name: Template Offre
 *
 *
 * @package vatier
 */

get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

	<?php $post_thumbnail_id = get_post_thumbnail_id(); ?>
	<?php $post_thumbnail_url = wp_get_attachment_url( $post_thumbnail_id ); ?>
	<?php if(empty($post_thumbnail_url)): ?>
	<?php $post_thumbnail_url = get_template_directory_uri().'/images/contact.jpg'; ?>
	<?php endif; ?>
	<?php $subtitle = get_field('sous_titre'); ?>
	<?php $icone = get_field('icone'); ?>

	<div class="section-top-image">
		<div class="container inner">
			<?php if(!empty($icone)): ?>
			<div class="row">
				<div class="page-icone"><img alt="<?php echo get_the_title(); ?>" src="<?php echo $icone; ?>" /></div><!--
				--><div class="page-title-box">
					<h1 class="page-title"><?php echo get_the_title(); ?></h1>
					<?php if(!empty($subtitle)): ?>
					<h2 class="page-subtitle"><?php echo $subtitle; ?></h2>
					<?php endif; ?>
				</div>
			</div>
			<?php else: ?>
			<h1 class="page-title"><?php echo get_the_title(); ?></h1>
			<?php if(!empty($subtitle)): ?>
			<h2 class="page-subtitle"><?php echo $subtitle; ?></h2>
			<?php endif; ?>
			<?php endif; ?>
		</div>
	</div>

	<div class="section-black">
		<div class="container inner">
			<div class="filter-btn-group clearfix txtcenter">
				<div class="filter-btn-box">
					<button class="filter-btn active" data-filter="*"><?php _e('Toutes', 'vatier'); ?></button>
				</div>
				<?php $categories = get_terms( 'filtrage', array('hide_empty' => 0) ); ?>
				<?php foreach ($categories as $category): ?>
				<div class="filter-btn-box">
					<button class="filter-btn" data-filter=".<?php echo $category->slug; ?>"><?php echo $category->name; ?></button>
				</div>
				<?php endforeach; ?>
			</div>
		</div>
	</div>

	<div id="primary" class="content-area">
		<div id="main" class="container inner" role="main">
			<div class="offre-gallery clearfix">
			<?php $args = array('posts_per_page' => '-1', 'post_type' => 'offre_bloc', 'post_status' => 'publish', 'orderby' => 'title', 'order' => 'ASC', 'suppress_filters' => false); ?>
		    <?php $posts = get_posts($args); ?>
		    <?php $index = 1; ?>
		    <?php $cpt = 1; ?>
		    <?php $image_num = 1; ?>
		    <?php $bg_arr = array( 'offre-white', 'offre-gray', 'offre-gray-light'); ?>
		    <?php foreach ($posts as $offre): ?>
		    	<?php $class_color = ""; ?>
		    	<?php
		    		if($index == 1)
		    		{
		    			$class_color = "offre-white";
		    		}
		    		elseif($index == 2)
		    		{
		    			$class_color = "offre-gray-light";
		    		}
		    		elseif($index == 3)
		    		{
		    			$class_color = "offre-gray";
		    		}
		    		elseif($index == 4){
		    			$class_color = "offre-gray-light";
		    		}
		    		else
		    		{
		    			$class_color = $bg_arr[$index % 3];
		    		}

		    	?>

		    	<?php $terms = get_the_terms($offre->ID, 'filtrage'); ?>
		    	<?php $filtrage = ''; ?>
		    	<?php if(isset($terms[0])): ?>
		    	<?php
		    		foreach($terms as $term){
		    			$filtrage .= ' '.$term->slug;
		    		}
		    	?>
		    	<?php endif; ?>
		       	<?php $offre_thumbnail_id = get_post_thumbnail_id($offre->ID); ?>
				<?php $offre_thumbnail_url = wp_get_attachment_url( $offre_thumbnail_id ); ?>
				<div class="offre-gallery-item offre-gallery-p-item txtcenter <?php echo $class_color; ?> w33 fl<?php echo $filtrage; ?>">
					<div class="offre-gallery-item-content">
						<div class="offre-icone"><img alt="<?php echo $offre->post_title; ?>" src="<?php echo $offre_thumbnail_url; ?>" /></div>
						<p class="offre-title"><?php echo $offre->post_title; ?></p>
						<div class="offre-hover-content txtleft">
							<p class="offre-hover-title">
								<img class="offre-hover-icone verti-middle" alt="<?php echo $offre->post_title; ?>" src="<?php echo $offre_thumbnail_url; ?>" />
								<span class="verti-middle"><?php echo $offre->post_title; ?></span>
							</p>
							<div class="offre-hover-body">
								<a class="bl" href="<?php echo get_the_permalink($offre->ID); ?>">
								<?php echo apply_filters('the_content', $offre->post_content); ?>
								</a>
							</div>
						</div>
					</div>
				</div>
				<?php if($index == 4): ?>
				<div class="offre-gallery-item txtcenter w33 fl">
					<img  class="offre-images" alt="Offre" src="<?php echo get_template_directory_uri(); ?>/images/offre-img-1.jpg" />
				</div>
				<?php endif; ?>
				<?php if($index > 4 && $cpt % 3 == 0): ?>
				<?php if($image_num == 1) $image_num = 2; else $image_num = 1; ?>
				<div class="offre-gallery-item txtcenter w33 fl">
					<img class="offre-images" alt="Offre" src="<?php echo get_template_directory_uri(); ?>/images/offre-img-<?php echo $image_num; ?>.jpg" />
				</div>
				<?php endif; ?>
				<?php if($index > 4): ?>
				<?php $cpt++; ?>
				<?php endif; ?>
				<?php $index++; ?>
		    <?php endforeach; ?>
			</div>
		</div>
	</div>
<?php endwhile; ?>

<script type="text/javascript">
jQuery(document).ready(function(){
	jQuery(".section-top-image").backstretch("<?php echo $post_thumbnail_url; ?>");	
});

jQuery(window).load(function(){
	var grid = jQuery('.offre-gallery').isotope({
		percentPosition: true,
		layoutMode: 'fitRows',
	});

	jQuery('.filter-btn').on( 'click', function() {
	  	var filterValue = jQuery(this).attr('data-filter');
	  	jQuery('.filter-btn').removeClass('active');
	  	jQuery(this).addClass('active');
	  	grid.isotope({ filter: filterValue });
	});

	jQuery('.offre-gallery-item').matchHeight({byRow: false});

	jQuery('.offre-gallery-item-content').hover(function(){
		jQuery('.offre-hover-content', this).stop(true, false).fadeIn();
	}, function(){
		jQuery('.offre-hover-content', this).stop(true, false).fadeOut();
	});
});
</script>

<?php get_footer(); ?>
