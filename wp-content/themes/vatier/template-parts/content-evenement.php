<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package vatier
 */

?>
<?php $post_thumbnail_id = get_post_thumbnail_id(18); ?>
<?php $post_thumbnail_url = wp_get_attachment_url( $post_thumbnail_id ); ?>
<?php if(empty($post_thumbnail_url)): ?>
<?php $post_thumbnail_url = get_template_directory_uri().'/images/contact.jpg'; ?>
<?php endif; ?>
<?php $subtitle = get_field('sous_titre', 18); ?>
<?php $icone = get_field('icone', 18); ?>

<div class="section-top-image">
	<div class="container inner">
		<?php if(!empty($icone)): ?>
		<div class="row">
			<div class="page-icone"><img alt="<?php echo get_the_title(); ?>" src="<?php echo $icone; ?>" /></div><!--
			--><div class="page-title-box">
				<h1 class="page-title"><?php echo get_the_title(18); ?></h1>
				<?php if(!empty($subtitle)): ?>
				<h2 class="page-subtitle"><?php echo $subtitle; ?></h2>
				<?php endif; ?>
			</div>
		</div>
		<?php else: ?>
		<h1 class="page-title"><?php echo get_the_title(18); ?></h1>
		<?php if(!empty($subtitle)): ?>
		<h2 class="page-subtitle"><?php echo $subtitle; ?></h2>
		<?php endif; ?>
		<?php endif; ?>
	</div>
</div>


<?php $year_arr = array(); ?>
<?php $month_arr = array('01' => 'Janvier', '02' => 'Février', '03' => 'Mars', '04' => 'Avril', '05' => 'Mai', '06' => 'Juin', '07' => 'Juillet', '08' => 'Aout', '09' => 'Septembre', '10' => 'Octobre', '11' => 'Novembre', '12' => 'Décembre'); ?>
<?php $args = array('posts_per_page' => '-1', 'post_type' => 'evenement', 'post_status' => 'publish', 'meta_key' => 'evenement_date', 'orderby' => 'meta_value_num', 'order' => 'ASC', 'suppress_filters' => false); ?>
<?php $posts = get_posts($args); ?>
<?php $posts_arr_date = array(); ?>
<?php foreach ($posts as $post): ?>
<?php $date = get_field('evenement_date', $post->ID); ?>
<?php list($year, $month, $day) =explode("-",$date); ?>
<?php $posts_arr_date[$year][$month][$day][] = $post->ID; ?>
<?php ksort($posts_arr_date[$year]); ?>
<?php ksort($posts_arr_date[$year][$month]); ?>
<?php if(!in_array($year, $year_arr)) $year_arr[] = $year; ?>
<?php $year_arr = array_unique($year_arr); ?>
<?php endforeach; ?>
<?php wp_reset_postdata(); ?>

<?php $date_event = get_field('evenement_date'); ?>
<?php list($year_event, $month_event, $day_event) =explode("-",$date_event); ?>
<?php $meta_query = array('relation' => 'AND', array('key' => 'evenement_date', 'value' => $year_event.'-'.$month_event.'-', 'compare' => 'LIKE' )); ?>
<?php $args = array('posts_per_page' => '-1', 'post_type' => 'evenement', 'post_status' => 'publish', 'meta_key' => 'evenement_date', 'orderby' => 'meta_value_num', 'order' => 'ASC', 'meta_query' => $meta_query, 'suppress_filters' => false); ?>
<?php $posts_ordred = array(); ?>
<?php $posts = get_posts($args); ?>
<?php foreach ($posts as $post): ?>
<?php $date = get_field('evenement_date', $post->ID); ?>
<?php $posts_ordred[$date][] = $post; ?>
<?php endforeach; ?>
<?php ksort($posts_ordred); ?>
<?php wp_reset_postdata(); ?>

<div id="primary" class="content-area ptl pbl">
	<div id="main" class="container inner" role="main">
		<div class="filtrage-date-box">
			<div id="tabs">
			 	<ul class="filtrage-years clearfix">
			 		<?php foreach ($year_arr as $year): ?>
					<li class="filtrage-year-item"><a class="filtrage-year-btn" href="#tabs-<?php echo $year; ?>"><?php echo $year; ?></a></li>
					<?php endforeach; ?>
			  	</ul>

			  	<?php foreach ($year_arr as $year): ?>
			  	<div id="tabs-<?php echo $year; ?>">
			    	<ul class="filtrage-months clearfix">
			    		
						<?php foreach ($month_arr as $key => $month): ?>
						<li class="filtrage-month-item">
							<?php if(isset($posts_arr_date[$year][$key])): ?>
							<a class="filtrage-month-btn<?php if($year_event == $year && $month_event == $key) echo ' active'; ?>" href="<?php echo get_the_permalink(array_values($posts_arr_date[$year][$key])[0][0]); ?>"><?php echo $month; ?></a>
							<?php else: ?>
							<a class="filtrage-month-btn<?php if($year_event == $year && $month_event == $key) echo ' active'; ?>" href="javascript:void(0);"><?php echo $month; ?></a>
							<?php endif; ?>
						</li>
						<?php endforeach; ?>
					</ul>
			  	</div>
			  	<?php endforeach; ?>
			</div>
		</div>
		<hr />

		<div class="row">
			<div class="news-menu-box w30 prm">
				<p class="news-menu-title"><?php _e('Les événements de', 'vatier'); ?><br><span class="txt-rouge"><?php echo $month_arr[$month_event].' '.$year_event; ?></span></p>
				<ul class="news-menu">
					<?php foreach ($posts_ordred as $post_arr): ?>
					<?php foreach ($post_arr as $post): ?>
					<li class="news-menu-item">
						<p class="article-date"><?php echo get_field('evenement_date', $post->ID); ?></p>
						<hr class="separator-rouge" />
						<p class="news-menu-link"><a href="<?php the_permalink($post->ID); ?>"><?php echo get_the_title($post->ID); ?></a></p>
					</li>
					<?php endforeach; ?>
					<?php endforeach; ?>
					<?php wp_reset_postdata(); ?>
				</ul>
			</div><!--
			--><div class="article-content-box plm">

				<div class="mobile-article-select-box">
					<p class="news-menu-title"><?php _e('Les événements de', 'vatier'); ?> <span class="txt-rouge"><?php echo $month_arr[$month_event].' '.$year_event; ?></span></p>
					<select id="mobile-article-select">
						<?php foreach ($posts_ordred as $post_arr): ?>
						<?php foreach ($post_arr as $post): ?>
						<option value="<?php the_permalink(); ?>"><?php the_title(); ?></option>
						<?php endforeach; ?>
						<?php endforeach; ?>
						<?php wp_reset_postdata(); ?>
					</select>
				</div>

				<article class="article-content">
					<?php if ( function_exists( 'ADDTOANY_SHARE_SAVE_KIT' ) ) { ADDTOANY_SHARE_SAVE_KIT(); } ?>
					<header class="article-header">
	    				<p class="article-date"><?php echo $year_event.' | '.$month_event.' | '.$day_event; ?></p>
	    				<hr class="separator-rouge" />
	    				<h2 class="article-title"><?php echo get_the_title(); ?></h2>
    				</header>
    				<div class="article-body">
    					<?php $news_thumbnail_id = get_post_thumbnail_id(); ?>
						<?php $news_thumbnail_url = wp_get_attachment_url( $news_thumbnail_id ); ?>
						<div class="article-img-box"><img class="w100 bl" alt="<?php echo get_the_title(); ?>" src="<?php echo $news_thumbnail_url; ?>" /></div>
						<?php the_content(); ?>
    				</div>
    			</article>
			</div>
		</div>

		<hr class="mtl">

		<div class="filtrage-date-box">
			<div id="tabs-bottom">
			 	<ul class="filtrage-years clearfix">
			 		<?php foreach ($year_arr as $year): ?>
					<li class="filtrage-year-item"><a class="filtrage-year-btn" href="#tabs-bottom-<?php echo $year; ?>"><?php echo $year; ?></a></li>
					<?php endforeach; ?>
			  	</ul>

			  	<?php foreach ($year_arr as $year): ?>
			  	<div id="tabs-bottom-<?php echo $year; ?>">
			    	<ul class="filtrage-months clearfix">
			    		
						<?php foreach ($month_arr as $key => $month): ?>
						<li class="filtrage-month-item">
							<?php if(isset($posts_arr_date[$year][$key])): ?>
							<a class="filtrage-month-btn<?php if($year_event == $year && $month_event == $key) echo ' active'; ?>" href="<?php echo get_the_permalink(array_values($posts_arr_date[$year][$key])[0][0]); ?>"><?php echo $month; ?></a>
							<?php else: ?>
							<a class="filtrage-month-btn<?php if($year_event == $year && $month_event == $key) echo ' active'; ?>" href="javascript:void(0);"><?php echo $month; ?></a>
							<?php endif; ?>
						</li>
						<?php endforeach; ?>
					</ul>
			  	</div>
			  	<?php endforeach; ?>
			</div>
		</div>

	</div>
</div>
<script type="text/javascript">
jQuery(document).ready(function(){
	jQuery(".section-top-image").backstretch("<?php echo $post_thumbnail_url; ?>");
	var index = <?php echo array_search($year_event, $year_arr); ?>;
	jQuery('#tabs').tabs({
		active: index,
	});

	jQuery('#tabs-bottom').tabs({
		active: index,
	});
});
</script>