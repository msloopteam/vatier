<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package vatier
 */

?>
<?php $pid = get_the_ID(); ?>

<?php $args = array('posts_per_page' => '-1', 'post_type' => 'equipe', 'post_status' => 'publish', 'orderby' => 'title', 'order' => 'ASC', 'suppress_filters' => false); ?>
<?php $posts = get_posts($args); ?>
<?php $equipe_arr = array(); ?>
<?php foreach ($posts as $equipe): ?>
<?php $equipe_arr[] = $equipe->ID; ?>
<?php endforeach; ?>
<?php $key = array_search($pid, $equipe_arr); ?>
			
<?php $top_image = get_field('photo'); ?>
<?php $fonction = ''; ?>
<?php $terms = get_the_terms(get_the_ID(), 'fonction'); ?>
<?php if(isset($terms[0])) $fonction = $terms[0]; ?>

<?php if(!empty($top_image)): ?>
<div class="section-top-photo">
	<img class="equipe-top-photo" alt="<?php echo get_the_title(); ?>" src="<?php echo $top_image; ?>" />
	<a class="nav-prev"<?php if(isset($equipe_arr[$key-1])) echo ' href="'.get_the_permalink($equipe_arr[$key-1]).'"' ?>><img alt="prev" src="<?php echo get_template_directory_uri(); ?>/images/prev-e.png" /></a>
	<a class="nav-next"<?php if(isset($equipe_arr[$key+1])) echo ' href="'.get_the_permalink($equipe_arr[$key+1]).'"' ?>><img alt="next" src="<?php echo get_template_directory_uri(); ?>/images/next-e.png" /></a>
</div>
<?php endif; ?>

<div id="primary" class="content-area ptl pbl">
	<div id="main" class="container inner" role="main">
		<div class="row equipe-top-section">
			<div class="w70 prl left-col">
				<h2 class="person-name"><?php echo get_the_title(); ?></h2>
				<p class="person-fonction"><?php echo isset($fonction->name) ? $fonction->name : ""; ?></p>
				<div class="person-description"><?php echo apply_filters('the_content', get_field('description')); ?></div>
			</div><!--
			--><div class="w30 pts plm right-col">
				<?php $person_email = get_field('email'); ?>
				<?php if(!empty($person_email)): ?>
				<p class="person-contact-infos"><strong><?php _e('Email:', 'vatier'); ?></strong><br><?php echo $person_email; ?></p>
				<?php endif; ?>

				<?php $person_telephone = get_field('telephone'); ?>
				<?php if(!empty($person_telephone)): ?>
				<p class="person-contact-infos"><strong><?php _e('Téléphone:', 'vatier'); ?></strong><br><?php echo $person_telephone; ?></p>
				<?php endif; ?>

				<?php $person_telecopie = get_field('telecopie'); ?>
				<?php if(!empty($person_telecopie)): ?>
				<p class="person-contact-infos"><strong><?php _e('Télécopie:', 'vatier'); ?></strong><br><?php echo $person_telecopie; ?></p>
				<?php endif; ?>
			</div>
		</div>
		<div class="row equipe-bottom-section">
			<div class="w70 prl left-col">
				<?php the_content(); ?>
			</div><!--
			--><div class="w30 plm right-col">
				<?php $domaines_intervention = get_field('domaines_intervention'); ?>
				<?php if(!empty($domaines_intervention)): ?>
				<div class="domaines-intervention">
					<p><strong><?php _e("Principaux domaines d’intervention", "vatier"); ?></strong></p>
					<?php echo apply_filters('the_content', $domaines_intervention); ?>
				</div>
				<?php endif; ?>

				<?php $equipe_directe = get_field('equipe_directe'); ?>

				<?php if(!empty($equipe_directe)): ?>
				<div class="equipe-directe">
					<p><strong><?php _e("Equipe directe", "vatier"); ?></strong></p>
					<?php foreach ($equipe_directe as $equipe): ?>
					<p><a href="<?php echo get_the_permalink($equipe); ?>"><?php echo get_the_title($equipe); ?></a></p>
					<?php endforeach; ?>
				</div>
				<?php endif; ?>

				<?php $experiences = get_field('experiences'); ?>
				<?php if(!empty($experiences)): ?>
				<div class="experiences">
					<?php echo apply_filters('the_content', $experiences); ?>
				</div>
				<?php endif; ?>
			</div>
		</div>

		<div class="equipe-pager mtl mbl clearfix">
			<?php if(isset($equipe_arr[$key-1])): ?>
			<div class="pager-prev-box fl">
				<a class="pager-prev-link clearfix" href="<?php echo get_the_permalink($equipe_arr[$key-1]); ?>">
					<img class="img-prev" alt="prev" src="<?php echo get_template_directory_uri(); ?>/images/prev-e.png" />
					<div class="pager-prev-info">
						<p class="pager-person-name"><?php echo get_the_title($equipe_arr[$key-1]); ?></p>
						<?php $fonction_person = ''; ?>
						<?php $terms = get_the_terms($equipe_arr[$key-1], 'fonction'); ?>
						<?php if(isset($terms[0])) $fonction_person = $terms[0]->name; ?>
						<?php if(!empty($fonction_person)): ?>
						<p class="pager-person-fonction"><?php echo $fonction_person; ?></p>
						<?php endif; ?>
					</div>
				</a>
			</div>
			<?php endif; ?>

			<?php if(isset($equipe_arr[$key+1])): ?>
			<div class="pager-next-box fr">
				<a class="pager-next-link clearfix" href="<?php echo get_the_permalink($equipe_arr[$key+1]); ?>">
					<img class="img-next" alt="next" src="<?php echo get_template_directory_uri(); ?>/images/next-e.png" />
					<div class="pager-next-info">
						<p class="pager-person-name"><?php echo get_the_title($equipe_arr[$key+1]); ?></p>
						<?php $fonction_person = ''; ?>
						<?php $terms = get_the_terms($equipe_arr[$key+1], 'fonction'); ?>
						<?php if(isset($terms[0])) $fonction_person = $terms[0]->name; ?>
						<?php if(!empty($fonction_person)): ?>
						<p class="pager-person-fonction"><?php echo $fonction_person; ?></p>
						<?php endif; ?>
					</div>
				</a>
			</div>
			<?php endif; ?>
		</div>
	</div>
</div>