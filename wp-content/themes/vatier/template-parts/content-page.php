<?php
/**
 * Template part for displaying page content in page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package vatier
 */

?>
<?php $post_thumbnail_id = get_post_thumbnail_id(); ?>
<?php $post_thumbnail_url = wp_get_attachment_url( $post_thumbnail_id ); ?>
<?php if(empty($post_thumbnail_url)): ?>
<?php $post_thumbnail_url = get_template_directory_uri().'/images/contact.jpg'; ?>
<?php endif; ?>
<?php $subtitle = get_field('sous_titre'); ?>
<?php $icone = get_field('icone'); ?>

<div class="section-top-image">
	<div class="container inner">
		<?php if(!empty($icone)): ?>
		<div class="row">
			<div class="page-icone"><img alt="<?php echo get_the_title(); ?>" src="<?php echo $icone; ?>" /></div><!--
			--><div class="page-title-box">
				<h1 class="page-title"><?php echo get_the_title(); ?></h1>
				<?php if(!empty($subtitle)): ?>
				<h2 class="page-subtitle"><?php echo $subtitle; ?></h2>
				<?php endif; ?>
			</div>
		</div>
		<?php else: ?>
		<h1 class="page-title"><?php echo get_the_title(); ?></h1>
		<?php if(!empty($subtitle)): ?>
		<h2 class="page-subtitle"><?php echo $subtitle; ?></h2>
		<?php endif; ?>
		<?php endif; ?>
	</div>
</div>

<div class="section-gray-light">
	<div class="container inner">
		<div class="pal normal-page-content"><?php the_content(); ?></div>
	</div>
</div>

<script type="text/javascript">
jQuery(document).ready(function(){
	jQuery(".section-top-image").backstretch("<?php echo $post_thumbnail_url; ?>");
});
</script>