<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package vatier
 */

?>

<?php $post_thumbnail_id = get_post_thumbnail_id(14); ?>
<?php $post_thumbnail_url = wp_get_attachment_url( $post_thumbnail_id ); ?>
<?php if(empty($post_thumbnail_url)): ?>
<?php $post_thumbnail_url = get_template_directory_uri().'/images/contact.jpg'; ?>
<?php endif; ?>
<?php $subtitle = get_field('sous_titre', 14); ?>
<?php $icone = get_field('icone', 14); ?>

<div class="section-top-image">
	<div class="container inner">
		<?php if(!empty($icone)): ?>
		<div class="row">
			<div class="page-icone"><img alt="<?php echo get_the_title(); ?>" src="<?php echo $icone; ?>" /></div><!--
			--><div class="page-title-box">
				<h1 class="page-title"><?php echo get_the_title(14); ?></h1>
				<?php if(!empty($subtitle)): ?>
				<h2 class="page-subtitle"><?php echo $subtitle; ?></h2>
				<?php endif; ?>
			</div>
		</div>
		<?php else: ?>
		<h1 class="page-title"><?php echo get_the_title(14); ?></h1>
		<?php if(!empty($subtitle)): ?>
		<h2 class="page-subtitle"><?php echo $subtitle; ?></h2>
		<?php endif; ?>
		<?php endif; ?>
	</div>
</div>

<div id="primary" class="content-area ptl pbl">
	<div id="main" class="container inner" role="main">
		<div class="row">
			<div class="news-menu-box w30 prm">
				<p class="news-menu-title"><?php _e('Les dernières actus', 'vatier'); ?></p>
				<ul class="news-menu">
					<?php $show_posts = '10'; ?>
					<?php $args = array('posts_per_page' => $show_posts, 'paged' => 1, 'post_type' => 'post', 'post_status' => 'publish', 'meta_key' => 'type', 'meta_value' => 'actualite', 'orderby' => 'date', 'order' => 'DESC', 'suppress_filters' => false); ?>
	    			<?php $query = new WP_Query($args); ?>

	    			<?php while($query->have_posts()): ?>

					<?php $query->the_post(); ?>

					<li class="news-menu-item">
						<p class="article-date"><?php echo get_the_date('d | m | Y'); ?></p>
						<hr class="separator-rouge" />
						<p class="news-menu-link"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></p>
					</li>

					<?php endwhile; ?>
					
				</ul>
				<?php $pages = $query->max_num_pages; ?>
				
				<?php if($pages > 1) :?>
				<?php $cpt = 1; ?>
				<div class="news-pager-box clearfix txtcenter mtm">
					<a class="news-pager-prev" href="javascript:void(0);"><img alt="Prev" src="<?php echo get_template_directory_uri(); ?>/images/prev-gray.png" /><br><?php _e('Préc', 'vatier'); ?></a>
					<?php for($i=1; $i<=$pages; $i++): ?>
					<a id="pager-<?php echo $i; ?>" class="news-menu-pager<?php if($cpt==1) echo ' active'; ?>" href="javascript:void(0);" data-page="<?php echo $i; ?>">
						<img class="pager-active-img" alt="<?php echo $i; ?>" src="<?php echo get_template_directory_uri() ?>/images/pager-active.png" />
						<img class="pager-img" alt="<?php echo $i; ?>" src="<?php echo get_template_directory_uri() ?>/images/pager.png" />
					</a>
					<?php $cpt++; ?>
					<?php endfor; ?>
					<a class="news-pager-next" href="javascript:void(0);"><img alt="Next" src="<?php echo get_template_directory_uri(); ?>/images/next-gray.png" /><br><?php _e('Suiv', 'vatier'); ?></a>
				</div>
				<?php endif; ?>

				<?php wp_reset_query(); ?>
			</div><!--
			--><div class="article-content-box w70 plm">

				<div class="mobile-article-select-box">
					<p class="news-menu-title"><?php _e('Les dernières actus', 'vatier'); ?></p>
					<select id="mobile-article-select">
						<?php $args = array('posts_per_page' => '-1', 'paged' => 1, 'post_type' => 'post', 'post_status' => 'publish', 'meta_key' => 'type', 'meta_value' => 'actualite', 'orderby' => 'date', 'order' => 'DESC', 'suppress_filters' => false); ?>
		    			<?php $query = new WP_Query($args); ?>

		    			<?php while($query->have_posts()): ?>

						<?php $query->the_post(); ?>
						<option value="<?php the_permalink(); ?>"><?php the_title(); ?></option>
						<?php endwhile; ?>
						<?php wp_reset_query(); ?>
					</select>
				</div>

				<article class="article-content">
					<?php if ( function_exists( 'ADDTOANY_SHARE_SAVE_KIT' ) ) { ADDTOANY_SHARE_SAVE_KIT(); } ?>
					<header class="article-header">
	    				<p class="article-date"><?php echo get_the_date('d | m | Y'); ?></p>
	    				<hr class="separator-rouge" />
	    				<h2 class="article-title"><?php echo get_the_title(); ?></h2>
    				</header>
    				<div class="article-body">
    					<?php $news_thumbnail_id = get_post_thumbnail_id(); ?>
						<?php $news_thumbnail_url = wp_get_attachment_url( $news_thumbnail_id ); ?>
						<div class="article-img-box"><img class="w100 bl" alt="<?php echo get_the_title(); ?>" src="<?php echo $news_thumbnail_url; ?>" /></div>
						<?php the_content(); ?>
    				</div>
    			</article>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
var paged = 1;
var maxp = <?php echo $pages ?>;
jQuery(document).ready(function(){
	jQuery(".section-top-image").backstretch("<?php echo $post_thumbnail_url; ?>");
	<?php if($pages > 1) :?>
	jQuery('.news-menu-pager').on('click', function(event){
		var pnum = jQuery(this).data('page');
		if(pnum != paged)
		{
			jQuery('.news-menu-pager').removeClass('active');
			jQuery(this).addClass('active');
			paged = pnum;

			var param = {'type':'actualite', 'paged':paged};

			jQuery.post(
			    "<?php echo admin_url( 'admin-ajax.php' ); ?>",
			    {
			        'action': 'vatier_get_article_menu',
			        'param': param,
			    },
			    function(response){
		            jQuery('.news-menu').html(response);
		            jQuery("html, body").animate({ scrollTop: jQuery('#main').offset().top }, 1000);
		        }
			);
		}
	});

	jQuery('.news-pager-next').on('click', function(event){
		if(paged < maxp)
		{
			jQuery('.news-menu-pager').removeClass('active');
			paged++;
			jQuery('#pager-'+paged).addClass('active');
			
			var param = {'type':'actualite', 'paged':paged};

			jQuery.post(
			    "<?php echo admin_url( 'admin-ajax.php' ); ?>",
			    {
			        'action': 'vatier_get_article_menu',
			        'param': param,
			    },
			    function(response){
		            jQuery('.news-menu').html(response);
		            jQuery("html, body").animate({ scrollTop: jQuery('#main').offset().top }, 500);
		        }
			);
		}
	});

	jQuery('.news-pager-prev').on('click', function(event){
		if(paged > 1)
		{
			jQuery('.news-menu-pager').removeClass('active');
			paged--;
			jQuery('#pager-'+paged).addClass('active');

			var param = {'type':'actualite', 'paged':paged};

			jQuery.post(
			    "<?php echo admin_url( 'admin-ajax.php' ); ?>",
			    {
			        'action': 'vatier_get_article_menu',
			        'param': param,
			    },
			    function(response){
		            jQuery('.news-menu').html(response);
		            jQuery("html, body").animate({ scrollTop: jQuery('#main').offset().top }, 500);
		        }
			);
		}
	});
	<?php endif; ?>
});
</script>