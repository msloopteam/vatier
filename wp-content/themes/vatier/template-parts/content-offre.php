<?php
/**
 * Template part for displaying page content in page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package vatier
 */

?>
<?php $post_thumbnail_id = get_post_thumbnail_id(12); ?>
<?php $post_thumbnail_url = wp_get_attachment_url( $post_thumbnail_id ); ?>
<?php if(empty($post_thumbnail_url)): ?>
<?php $post_thumbnail_url = get_template_directory_uri().'/images/contact.jpg'; ?>
<?php endif; ?>
<?php $subtitle = get_field('sous_titre', 12); ?>
<?php $icone = get_field('icone', 12); ?>

<div class="section-top-image">
	<div class="container inner">
		<?php if(!empty($icone)): ?>
		<div class="row">
			<div class="page-icone"><img alt="<?php echo get_the_title(); ?>" src="<?php echo $icone; ?>" /></div><!--
			--><div class="page-title-box">
				<h1 class="page-title"><?php echo get_the_title(12); ?></h1>
				<?php if(!empty($subtitle)): ?>
				<h2 class="page-subtitle"><?php echo $subtitle; ?></h2>
				<?php endif; ?>
			</div>
		</div>
		<?php else: ?>
		<h1 class="page-title"><?php echo get_the_title(12); ?></h1>
		<?php if(!empty($subtitle)): ?>
		<h2 class="page-subtitle"><?php echo $subtitle; ?></h2>
		<?php endif; ?>
		<?php endif; ?>
	</div>
</div>

<div class="section-black">
	<div class="container inner">
		<h2 class="offre-page-title">
			<?php $icone_blanc = get_field('icone_blanc'); ?>
			<?php if(!empty($icone_blanc)): ?>
			<img class="verti-middle mrm" alt="<?php echo get_the_title(); ?>" src="<?php echo $icone_blanc; ?>" />
			<?php endif; ?>
			<span class="verti-middle"><?php echo get_the_title(); ?></span>
		</h2>
	</div>
</div>

<div class="section-gray-light">
	<div class="container inner">
		<div class="row">
			<div class="offre-page-content w70 prl">
				<?php $offre_contenu = get_field('offre_contenu'); ?>
				<?php echo apply_filters('the_content', $offre_contenu ); ?>
			</div><!--
			--><div class="offre-page-avocats w30">
				<?php $avocats_en_charge = get_field('avocats_en_charge'); ?>

				<?php if(!empty($avocats_en_charge)): ?>
				<h3 class="offre-avocats-title"><?php _e('Avocats en charge', 'vatier'); ?></h3>
				<ul class="offre-avocats-en-charge">
					<?php foreach ($avocats_en_charge as $avocat): ?>
					<li class="offre-avocat-en-charge bl">
						<?php $avocat_thumbnail_id = get_post_thumbnail_id($avocat->ID); ?>
						<?php $avocat_thumbnail_url = wp_get_attachment_url($avocat_thumbnail_id); ?>
						<a class="row" href="<?php echo get_the_permalink($avocat->ID); ?>">
						<div class="30 offre-avocat-en-charge-photo verti-middle">
							<img alt="<?php echo $avocat->post_title; ?>" src="<?php echo $avocat_thumbnail_url; ?>" />
						</div><!--
						--><div class="w70 plm offre-avocat-en-charge-infos verti-middle">
							<h4 class="offre-avocat-name"><?php echo $avocat->post_title; ?></h4>
							<?php $terms = get_the_terms($avocat->ID, 'fonction'); ?>
					    	<?php $fonction = ''; ?>
					    	<?php if(isset($terms[0])) $fonction = $terms[0]; ?>
							<p class="offre-avocat-fonction"><?php echo $fonction->name; ?></p>
						</div>
						</a>
					</li>
					<?php endforeach; ?>
				</ul>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>


<script type="text/javascript">
jQuery(document).ready(function(){
	jQuery(".section-top-image").backstretch("<?php echo $post_thumbnail_url; ?>");	
});
</script>