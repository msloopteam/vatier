<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package vatier
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php
		while ( have_posts() ) : the_post();
			$post_type = get_post_type();
			switch ($post_type) {
				case 'post':
					$type = get_field('type');
					switch ($type) {
						case 'actualite':
							get_template_part( 'template-parts/content', 'actualite' );
							break;
						case 'publication':
							get_template_part( 'template-parts/content', 'publication' );
							break;
						default:
							get_template_part( 'template-parts/content', 'actualite' );
							break;
					}
					break;
				case 'equipe':
					get_template_part( 'template-parts/content', 'equipe' );
					break;
				case 'evenement':
					get_template_part( 'template-parts/content', 'evenement' );
					break;
				case 'offre_bloc':
					get_template_part( 'template-parts/content', 'offre' );
					break;
				default:
					
					break;
			}
			
		endwhile;
		?>

		</main>
	</div>

<?php get_footer(); ?>
