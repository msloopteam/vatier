<?php
/**
 * Template Name: Template Contact
 *
 *
 * @package vatier
 */

get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

	<?php $post_thumbnail_id = get_post_thumbnail_id(); ?>
	<?php $post_thumbnail_url = wp_get_attachment_url( $post_thumbnail_id ); ?>
	<?php if(empty($post_thumbnail_url)): ?>
	<?php $post_thumbnail_url = get_template_directory_uri().'/images/contact.jpg'; ?>
	<?php endif; ?>
	<?php $subtitle = get_field('sous_titre'); ?>
	<?php $icone = get_field('icone'); ?>

	<div class="section-top-image">
		<div class="container inner">
			<?php if(!empty($icone)): ?>
			<div class="row">
				<div class="page-icone"><img alt="<?php echo get_the_title(); ?>" src="<?php echo $icone; ?>" /></div><!--
				--><div class="page-title-box">
					<h1 class="page-title"><?php echo get_the_title(); ?></h1>
					<?php if(!empty($subtitle)): ?>
					<h2 class="page-subtitle"><?php echo $subtitle; ?></h2>
					<?php endif; ?>
				</div>
			</div>
			<?php else: ?>
			<h1 class="page-title"><?php echo get_the_title(); ?></h1>
			<?php if(!empty($subtitle)): ?>
			<h2 class="page-subtitle"><?php echo $subtitle; ?></h2>
			<?php endif; ?>
			<?php endif; ?>
		</div>
	</div>

	<div id="primary" class="content-area">
		<div id="main" class="container inner" role="main">
			<div class="contact-row row">
				<div class="w50 contact-box">
					<p id="contact-paris" class="contact-ville upper"><?php _e('Paris', 'vatier'); ?></p>
					<div class="contact-info-box paris-info-box">
						<p class="mbm"><strong class="upper txt-rouge"><?php _e('Vatier paris', 'vatier'); ?></strong></p>
						<?php $contact_paris = get_option('contact_paris'); ?>
						<?php echo nl2br(do_shortcode($contact_paris)); ?>
					</div>
					<div id="contact-map-paris" class="contact-map"></div>
				</div><!--
				--><div class="w50 contact-box">
					<p id="contact-bruxelles" class="contact-ville upper"><?php _e('Bruxelles', 'vatier'); ?></p>
					<div class="contact-info-box bruxelles-info-box">
						<p class="mbm"><strong class="upper txt-white"><?php _e('Vatier Bruxelles', 'vatier'); ?></strong></p>
						<?php $contact_paris = get_option('contact_bruxelles'); ?>
						<?php echo nl2br(do_shortcode($contact_paris)); ?>
					</div>
					<div id="contact-map-bruxelles" class="contact-map"></div>
				</div>
			</div>

			<div class="section-red pam">
				<h2 class="section-red-title clearfix">
					<img class="verti-middle" alt="<?php _e('Nous rejoindre', 'vatier'); ?>" src="<?php echo get_template_directory_uri(); ?>/images/logo-white.png" /><span class="txt-white upper mls verti-middle"><?php _e('Nous rejoindre', 'vatier'); ?></span>
				</h2>
			</div>
			<div class="contact-content pbl ptl">
				<?php the_content(); ?>
			</div>
		</div>
		
	</div>
<?php endwhile; ?>

<script type="text/javascript">
jQuery(document).ready(function(){
	jQuery(".section-top-image").backstretch("<?php echo $post_thumbnail_url; ?>");
});

jQuery(window).load(function(){
	<?php $map_latitude_paris = get_option('map_latitude_paris'); ?>
	<?php $map_longitude_paris = get_option('map_longitude_paris'); ?>
	<?php $map_latitude_bruxelles = get_option('map_latitude_bruxelles'); ?>
	<?php $map_longitude_bruxelles = get_option('map_longitude_bruxelles'); ?>

	<?php if(!empty($map_latitude_paris) && !empty($map_longitude_paris)): ?>
		
	var location = new google.maps.LatLng(<?php echo $map_latitude_paris ?>, <?php echo $map_longitude_paris; ?>);

	var map_paris = new google.maps.Map(document.getElementById("contact-map-paris"), {
			        zoom: 15,
			        streetViewControl: false,
			        disableDefaultUI: true,
			        scrollwheel: false,
			        center: location,
			        mapTypeId: google.maps.MapTypeId.ROADMAP,
		      	});
	var marker = new MarkerWithLabel({
		            map: map_paris,
		            position: location,
		            animation: google.maps.Animation.DROP,
		            title: 'Vatier Paris',
		            labelAnchor: new google.maps.Point(<?php echo $map_latitude_paris ?>, <?php echo $map_longitude_paris; ?>),
		            labelClass: "google-labels", 
		            labelStyle: {opacity: 0.75}
	          	});
	
	google.maps.event.addListener(map_paris, 'tilesloaded', function() {
    	jQuery(".google-labels").text("Vatier Paris");
  	});

	<?php endif; ?>

	<?php if(!empty($map_latitude_bruxelles) && !empty($map_longitude_bruxelles)): ?>
	
	var location = new google.maps.LatLng(<?php echo $map_latitude_bruxelles ?>, <?php echo $map_longitude_bruxelles; ?>);

	var map_bruxelles = new google.maps.Map(document.getElementById("contact-map-bruxelles"), {
			        zoom: 15,
			        streetViewControl: false,
			        disableDefaultUI: true,
			        scrollwheel: false,
			        center: location,
			        mapTypeId: google.maps.MapTypeId.ROADMAP,
		      	});
	var marker = new MarkerWithLabel({
		            map: map_bruxelles,
		            position: location,
		            animation: google.maps.Animation.DROP,
		            title: 'Vatier Bruxelles',
		            labelAnchor: new google.maps.Point(<?php echo $map_latitude_paris ?>, <?php echo $map_longitude_paris; ?>),
		            labelClass: "google-labels", 
		            labelStyle: {opacity: 0.75}
	          	});
	
	google.maps.event.addListener(map_bruxelles, 'tilesloaded', function() {
    	jQuery(".google-labels").text("Vatier Bruxelles");
  	});

	<?php endif; ?> 
});

</script>

<?php get_footer(); ?>
